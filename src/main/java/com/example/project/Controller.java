package com.example.project;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("users")
public class Controller {
	private static Map<String, List<Student>> schooDB=new HashMap<String, List<Student>>();
	
	static {
		schooDB=new HashMap<String, List<Student>>();
		
		List<Student> lst=new ArrayList<Student>();
		Student std=new Student("sajal","class");
		lst.add(std);
		
		std=new Student("Sukesh","class 4");
		lst.add(std);
		
		schooDB.put("abc", lst);
		
		lst=new ArrayList<Student>();
		std=new Student("Kajal", "Class ");
		lst.add(std);
		
		std=new Student("anvesh","class");
		
		lst.add(std);
		
		schooDB.put("vyza", lst);
	}
	
	 @GetMapping(value = "/user/{schoolname}",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Student> getStudents(@PathVariable String schoolname){
		System.out.println("Getting student details for"+schoolname);
		
		List<Student> studentList=schooDB.get(schoolname);
		if(studentList==null) {
			studentList=new ArrayList<Student>();
			Student std=new Student("Not found","N/a");
			studentList.add(std);
		}
		return studentList;
	}
}

